'use strict';

const ccxws = require('ccxws');
const moment = require('moment');

// -------------------------------------------
const timeNowShort = () => moment(moment.utc()).format('DD/MMM HH:mm:ss:SSS');
// -------------------------------------------

module.exports = class ExchangeInstance {
    constructor({
        servType, exchange, forkN, eventLoggerFunc,
        exireRedis = 60 * 60 * 24, channel = 1,
        errorFunc, errorType, socketConfig = {},
    }) {
        this.exch = exchange;
        this.socketConfig = socketConfig;
        // -----------------------
        this.forkN = forkN;
        this.channel = channel;
        this.errorFunc = errorFunc;
        this.servType = servType;
        this.errorType = errorType;        
        // -----------------------
        this.socketInstance = null;
        this._init(exchange);
        // -----------------------
        this.eventLoggerFunc = eventLoggerFunc;
        this.exireRedis = exireRedis;
    }
    _init(exchange) {
        // -------------------------------------------
        switch (exchange) {
            case 'binance': this.socketInstance = new ccxws.Binance(this.socketConfig); break;
            case 'bithumb': this.socketInstance = new ccxws.Bithumb(this.socketConfig); break;
            case 'bitfinex': this.socketInstance = new ccxws.Bitfinex(this.socketConfig); break;
            case 'bitflyer': this.socketInstance = new ccxws.Bitflyer(this.socketConfig); break;
            case 'bitmex': this.socketInstance = new ccxws.BitMEX(this.socketConfig); break;
            case 'bitstamp': this.socketInstance = new ccxws.Bitstamp(this.socketConfig); break;
            case 'bittrex': this.socketInstance = new ccxws.Bittrex(this.socketConfig); break;
            case 'coinbasepro': this.socketInstance = new ccxws.CoinbasePro(this.socketConfig); break;
            case 'cexio': this.socketInstance = new ccxws.Cex(this.socketConfig); break;
            case 'ftx': this.socketInstance = new ccxws.Ftx(this.socketConfig); break;
            case 'hitbtc': this.socketInstance = new ccxws.HitBTC(this.socketConfig); break;
            case 'gateio': this.socketInstance = new ccxws.Gateio(this.socketConfig); break;
            case 'gemini': this.socketInstance = new ccxws.Gemini(this.socketConfig); break;
            case 'huobipro': this.socketInstance = new ccxws.Huobi(this.socketConfig); break;
            case 'kraken': this.socketInstance = new ccxws.Kraken(this.socketConfig); break;
            case 'kucoin': this.socketInstance = new ccxws.Kucoin(this.socketConfig); break;
            case 'poloniex': this.socketInstance = new ccxws.Poloniex(this.socketConfig); break;
            case 'liquid': this.socketInstance = new ccxws.Liquid(this.socketConfig); break;
            case 'okex': this.socketInstance = new ccxws.OKEx(this.socketConfig); break;

            default:
                console.log('No exchange', exchange);
                process.exit(0);
        }
        this._events();
    }
    _events() {
        const saveFunc = (type = '', params = {}) => {
            this.eventLoggerFunc(`ws_${this.servType}_${type}_${this.exch}_channel_${this.channel}_process_${this.forkN}`, {
                ...params,
                serviceTipe: this.servType,
                exchange: this.exch,
                channel: this.channel,
                fork: this.forkN,
                updTime: timeNowShort(),
                process: process.pid,
            }, this.exireRedis);
        }
        // ---------------- error -----------------------------------
        this.socketInstance.on('error', err => {
            saveFunc('error', { error: err.toString() });
            if (err.toString().includes(this.errorType)) {
                this.errorFunc();
            }
        });
        // ---------------- connected / disconnected -----------------------------------
        this.socketInstance.on('connected', () => {
            saveFunc('connected');
        });
        this.socketInstance.on('disconnected', () => {
            saveFunc('disconnected');
        });
        // ---------------- statuses -----------------------------------
        this.socketInstance.on('reconnecting', () => {
            saveFunc('reconnecting');
        });
        this.socketInstance.on('connecting', () => {
            saveFunc('connecting');
        });
        this.socketInstance.on('closing', () => {
            saveFunc('closing');
        });
        this.socketInstance.on('closed', () => {
            saveFunc('closed');
        });
    }
    _orderBookStep() {
        const obDepthStepFunc = (exch, tickerGroup) => {
            let step = 0;
            if (exch === 'huobipro') {
                switch (tickerGroup) {
                    case 'btcstb': step = 4; break;
                    case 'altstb': step = 0; break;
                    case 'altbtc': step = 0; break;
                    case 'altoth': step = 0; break;
                    case 'stbstb': step = 0; break;

                    default:
                        break;
                }
            };
            return step;
        }
    }
}